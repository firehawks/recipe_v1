﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericSearch
{
    public partial class Search
    {
        /* If data, keywords contains null, null ref exception will be thrown.
        Always true, if keywords contains the empty string */
        public static bool StringSearch(string[] data, string[] keywords)
        {
            bool temp = false;
            if (data != null && keywords != null)
            {
                for (int j = 0; j < keywords.Length; j++)
                {
                    temp = false;
                    if (keywords[j] != null)
                    {
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (data[i] != null && data[i].ToLower().Contains(keywords[j].ToLower()))
                            {
                                temp = true;
                                break;
                            }
                        }
                        if (!temp)
                            return false;
                    }
                }
            }
            return temp;
        }

        public static bool StringSearch(List<string> data, string[] keywords)
        {
            if (data != null && keywords != null)
                return (StringSearch(data.ToArray(), keywords));
            return false;
        }
    }
}
