﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Xml.Linq;
using System.Xml;
using GenericSearch;


namespace RecipesUnitTests
{
    [TestFixture]
    public class UnitTests
    {
        [Test]
        public void NullCollectionTest()
        {
            Assert.AreEqual(false, Search.StringSearch(null as string[], new string[0]));
            Assert.AreEqual(false, Search.StringSearch(new string[0], null as string[]));

            Assert.AreEqual(false, Search.StringSearch(null as List<string>, new string[0]));
            Assert.AreEqual(false, Search.StringSearch(new List<string>(), null as string[]));
        }

        [Test]
        public void NullDataTest()
        {
            Assert.AreEqual(false, Search.StringSearch(new string[1] { null }, new string[0]));
            Assert.AreEqual(false, Search.StringSearch(new string[0], new string[1] { null }));

            Assert.AreEqual(false, Search.StringSearch(new List<string>() { null }, new string[0]));
            Assert.AreEqual(false, Search.StringSearch(new List<string>(), new string[1] { null }));
        }

        [Test]
        public void SpecificCollectionTest()
        {
            string[] testKeywords1 = new string[1]
            {
                "collection"
            };
            string[] testKeywords2 = new string[2]
            {
                "skinless",
                "blue"
            };
            string[] testKeywords3 = new string[1]
            {
                "blue"
            };
            string[] testDataArray = new string[6]
            {
                "The quick brown fox jumped over the recipe collection.",
                "Sally sells seashells on the recipe collection.",
                "This collection is too long! But that's okay.",
                "Unit tests can be: red, green, brown, yellow, or purple.",
                "I want this unit test to be thoroughly tested!",
                "Only a small amount of oil is needed to saute meat. Option: Skinless, boneless chicken breasts may be used instead of beef cubes."
            };
            List<string> testDataList = new List<string>(testDataArray);

            Assert.AreEqual(true, Search.StringSearch(testDataArray, testKeywords1));
            Assert.AreEqual(true, Search.StringSearch(testDataList, testKeywords1));

            Assert.AreEqual(false, Search.StringSearch(testDataArray, testKeywords2));
            Assert.AreEqual(false, Search.StringSearch(testDataList, testKeywords2));

            Assert.AreEqual(false, Search.StringSearch(testDataArray, testKeywords3));
            Assert.AreEqual(false, Search.StringSearch(testDataList, testKeywords3));
        }

        [Test]
        public void CultureStringTest()
        {
            string[] testKeywords1 = new string[1]
            {
                "漢字"
            };
            string[] testKeywords2 = new string[1]
            {
                "漢声字"
            };
            string[] testDataArray = new string[3]
            {
                "The quick brown fox jumped over the recipe collection.",
                "Sally sells seashells on the recipe collection.",
                "漢字"
            };
            List<string> testDataList = new List<string>(testDataArray);

            Assert.AreEqual(true, Search.StringSearch(testDataArray, testKeywords1));
            Assert.AreEqual(true, Search.StringSearch(testDataList, testKeywords1));

            Assert.AreEqual(false, Search.StringSearch(testDataArray, testKeywords2));
            Assert.AreEqual(false, Search.StringSearch(testDataList, testKeywords2));
        }
    }
}
